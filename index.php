<?php get_header(); ?>
<main>
	<div class="container">
		<?php
		if ( function_exists( 'yoast_breadcrumb' ) ) {
			yoast_breadcrumb( '<nav id="breadcrumbs">', '</nav>' );
		}
		?>
		<?php while ( have_posts() ) : the_post(); ?>
			<h1><?php the_title(); ?></h1>
			<?php the_content(); ?>

		<?php endwhile; ?>
	</div>

</main>
<?php get_footer(); ?>
