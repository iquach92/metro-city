<?php

add_action( 'init', 'custom_post_type' );

function custom_post_type() {
	$labels = array(
		'name'               => _x( 'ЖК', 'metro-city' ),
		'singular_name'      => _x( 'ЖК', 'metro-city' ),
		'menu_name'          => _x( 'ЖК', 'metro-city' ),
		'name_admin_bar'     => _x( 'ЖК', 'metro-city' ),
		'add_new'            => __( 'Добавить новый', 'metro-city' ),
		'add_new_item'       => __( 'Добавить новый ЖК', 'metro-city' ),
		'new_item'           => __( 'Новый ', 'metro-city' ),
		'edit_item'          => __( 'Редактировать ', 'metro-city' ),
		'view_item'          => __( 'Посмотреть ', 'metro-city' ),
		'all_items'          => __( 'Все ', 'metro-city' ),
		'search_items'       => __( 'Search ', 'metro-city' ),
		'parent_item_colon'  => __( 'Parent :', 'metro-city' ),
		'not_found'          => __( 'No books found.', 'metro-city' ),
		'not_found_in_trash' => __( 'No books found in Trash.', 'metro-city' ),
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'show_in_rest'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'residential_complex' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'thumbnail', ),
	);


	register_post_type( 'residential_complex', $args );

	$args = array(
		'label'        => __( 'Класс Жилья', 'metro-city' ),
		'rewrite'      => false,
		'hierarchical' => true
	);

	register_taxonomy( 'class', 'residential_complex', $args );

	$args = array(
		'label'        => __( 'Дополнительные Опции', 'metro-city' ),
		'rewrite'      => false,
		'hierarchical' => true
	);

	register_taxonomy( 'options', 'residential_complex', $args );

	$args = array(
		'label'        => __( 'Срок Сдачи', 'metro-city' ),
		'rewrite'      => false,
		'hierarchical' => true
	);

	register_taxonomy( 'release', 'residential_complex', $args );
}
