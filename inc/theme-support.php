<?php
// Add meta title
add_theme_support( 'title-tag' );

// Add post thumbnail
add_theme_support( 'post-thumbnails' );
