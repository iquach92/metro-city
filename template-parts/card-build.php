<?php
$address = get_field( 'address' );
$release = get_field('release');
?>
<li class="page-loop__item wow animate__animated animate__fadeInUp" data-wow-duration="0.8s">

	<a href="#" class="favorites-link favorites-link__add" title="Добавить в Избранное" role="button">
		<span class="icon-heart"><span class="path1"></span><span class="path2"></span></span>
	</a>

	<a href="<?php the_permalink(); ?>" class="page-loop__item-link">
		<div class="page-loop__item-image">
			<?php if ( has_post_thumbnail() ) : ?>
				<?php the_post_thumbnail( 'rc-archive' ); ?>
			<?php else: ?>
				<img src="http://placehold.it/270x250" alt="<?php the_title(); ?>">
			<?php endif; ?>

			<div class="page-loop__item-badges">
				<span class="badge">Услуги 0%</span>
				<?php $category = get_the_terms(get_the_ID(), 'class') ?>
				<?php if ( $category ) : ?>
					<?php foreach ( $category as $item ) : ?>
						<span class="badge"><?php echo $item->name; ?></span>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</div>
		<div class="page-loop__item-info">
			<h3 class="page-title-h3"><?php the_title() ?></h3>

			<p><?php echo $release ?></p>

			<?php if ( have_rows( 'distance' ) ) : ?>
				<div class="page-text to-metro">
					<?php while ( have_rows( 'distance' ) ) : the_row();
						$metro = get_sub_field( 'metro' );
						$time = get_sub_field( 'time' );
						$as = get_sub_field( 'as' );
						$name = get_sub_field( 'name' ); ?>
						<div class="metro">
							<span class="icon-metro icon-metro--<?php echo $metro ?>"></span>
							<?php echo $name; ?>
							<span><?php echo $time; ?> <span class="<?php echo $as; ?>"></span></span>
						</div>
						<?php break; ?>
					<?php endwhile; ?>
				</div>
			<?php endif; ?>
			<span class="page-text text-desc"><?php echo $address ?></span>
		</div>
	</a>

</li>
