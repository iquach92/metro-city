<?php get_header(); ?>
<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post();
		$address = get_field( 'address' );
		$description = get_field( 'description' );
		$organization = get_field( 'organization' );
		$release = get_field( 'release' );
		?>
		<div class="container">

			<div class="page-top">

				<?php
				if ( function_exists( 'yoast_breadcrumb' ) ) {
					yoast_breadcrumb( '<nav id="breadcrumbs">', '</nav>' );
				}
				?>

			</div>

			<div class="page-section">

				<div class="page-content">

					<article class="post">

						<div class="post-header">

							<h1 class="page-title-h1"><?php the_title(); ?></h1>

							<?php if ( $organization ) : ?>
								<span><?php echo $organization ?></span>
							<?php endif; ?>

							<div class="post-header__details">
								<div class="address"><?php echo $address ?></div>
								<?php if ( have_rows( 'distance' ) ) : ?>
									<?php while ( have_rows( 'distance' ) ) : the_row();
										$metro = get_sub_field( 'metro' );
										$time = get_sub_field( 'time' );
										$as = get_sub_field( 'as' );
										$name = get_sub_field( 'name' ); ?>
										<div class="metro">
											<span class="icon-metro icon-metro--<?php echo $metro ?>"></span>
											<?php echo $name; ?>
											<span><?php echo $time; ?><span class="<?php echo $as; ?>"></span></span>
										</div>
									<?php endwhile; ?>
								<?php endif; ?>
							</div>

						</div>

						<div class="post-image">

							<?php if ( has_post_thumbnail() ) : ?>
								<?php the_post_thumbnail( 'rc-single' ); ?>
							<?php else: ?>
								<img src="http://placehold.it/870x400" alt="<?php the_title(); ?>">
							<?php endif; ?>


							<div class="page-loop__item-badges">
								<span class="badge">Услуги 0%</span>
								<?php $category = get_the_terms( get_the_ID(), 'class' ) ?>
								<?php if ( $category ) : ?>
									<?php foreach ( $category as $item ) : ?>
										<span class="badge"><?php echo $item->name; ?></span>
									<?php endforeach; ?>
								<?php endif; ?>
							</div>

							<a href="#" class="favorites-link favorites-link__add" title="Добавить в Избранное"
							   role="button">
								<span class="icon-heart"><span class="path1"></span><span class="path2"></span></span>
							</a>

						</div>

						<h2 class="page-title-h1"><?php _e( 'Характеристики ЖК', 'metro-city' ) ?></h2>

						<ul class="post-specs">
							<li>
								<span class="icon-building"></span>
								<div class="post-specs__info">
									<span>Класс жилья</span>
									<p>Комфорт</p>
								</div>
							</li>
							<li>
								<span class="icon-brick"></span>
								<div class="post-specs__info">
									<span>Конструктив</span>
									<p>Монолит-кирпич</p>
								</div>
							</li>
							<li>
								<span class="icon-paint"></span>
								<div class="post-specs__info">
									<span>Отделка</span>
									<p>
										Чистовая
										<span class="tip tip-info" data-toggle="popover" data-placement="top" data-content="And here's some amazing content. It's very engaging. Right?"><span class="icon-prompt"></span></span>
									</p>
								</div>
							</li>
							<li>
								<span class="icon-calendar"></span>
								<div class="post-specs__info">
									<span><?php _e( 'Срок сдачи', 'metro-city' ) ?></span>

									<p><?php echo $release; ?></p>

								</div>
							</li>
							<li>
								<span class="icon-ruller"></span>
								<div class="post-specs__info">
									<span>Высота потолков</span>
									<p>2,7 м</p>
								</div>
							</li>
							<li>
								<span class="icon-parking"></span>
								<div class="post-specs__info">
									<span>Подземный паркинг</span>
									<p>Присутствует</p>
								</div>
							</li>
							<li>
								<span class="icon-stair"></span>
								<div class="post-specs__info">
									<span>Этажность</span>
									<p>10-17</p>
								</div>
							</li>
							<li>
								<span class="icon-wallet"></span>
								<div class="post-specs__info">
									<span>Ценовая группа</span>
									<p>Выше среднего</p>
								</div>
							</li>
							<li>
								<span class="icon-rating"></span>
								<div class="post-specs__info">
									<span>Рейтинг</span>
									<p>8.8</p>
								</div>
							</li>
						</ul>
						<?php if ( $description ) : ?>
							<h2 class="page-title-h1"><?php _e( 'Краткое описание', 'metro-city' ) ?></h2>
							<div class="post-text">
								<?php echo $description ?>
							</div>
						<?php endif; ?>

						<h2 class="page-title-h1"><?php _e( 'Карта', 'metro-city' ) ?></h2>

						<div class="post-map" id="post-map" style="width: 100%; height: 300px;"></div>

					</article>

				</div>

				<div class="page-filter"></div>

			</div>

		</div>
	<?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>
