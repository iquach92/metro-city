<?php

// ajax filter

add_action( 'wp_ajax_nopriv_ajax_filter', 'ajax_filter' );
add_action( 'wp_ajax_ajax_filter', 'ajax_filter' );

function ajax_filter() {
	$houseArgs = array(
		'post_type'      => 'residential_complex',
		'posts_per_page' => - 1,
		'tax_query' => array(
			'relation' => 'AND',
		)
	);

	$deadline = $_POST['deadline'];
	if (!empty($deadline)):
		$houseArgs['tax_query'][] = array(
			'taxonomy' => 'release',
			'field' => 'slug',
			'terms' => $deadline,
		);
	endif;

	$category = $_POST['category'];
	if (!empty($category)) {
		$houseArgs['tax_query'][] = array(
			'taxonomy' => 'class',
			'field' => 'slug',
			'terms' => $category,
		);
	}
	$options = $_POST['options'];
	if (!empty($options)) {
		$houseArgs['tax_query'][] = array(
			'taxonomy' => 'options',
			'field' => 'slug',
			'terms' => $options,
			'operator' => 'AND'
		);
	}

	$house = new WP_Query( $houseArgs );
	?>
	<?php if ( $house->have_posts() ): ?>
		<?php while ( $house->have_posts() ): $house->the_post() ?>

			<?php  get_template_part( 'template-parts/card-build' ) ?>

		<?php endwhile; ?>
	<?php else: ?>
			<h2><?php _e('К сожалению по вашему запросу ничего не было найдено', 'metro-city') ?></h2>
	<?php endif; ?>
	<?php die(); ?>

<?php }

