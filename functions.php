<?php
// Scripts and styles
require_once ('inc/enqueue.php');
// Theme Support
require_once ('inc/theme-support.php');
// Register Post Types
require_once ('inc/post-types.php');
// Image Sizes
require_once ('inc/image-sizes.php');
// Ajax
require_once ('inc/ajax.php');

