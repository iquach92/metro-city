<?php // Template Name: Buildings ?>
<?php get_header(); ?>

<div class="container">

	<div class="page-top">

		<?php
		if ( function_exists( 'yoast_breadcrumb' ) ) {
			yoast_breadcrumb( '<nav id="breadcrumbs">', '</nav>' );
		}
		?>

		<div class="page-top__switchers">

			<div class="container">
				<div class="row">

					<div class="page-top__switchers-inner">

						<a href="#" class="page-top__filter">
							<span class="icon-filter"></span>
							Фильтры
						</a>

						<a href="#" data-tab-name="loop" class="page-top__switcher tab-nav active">
							<span class="icon-grid"></span>
						</a>

						<a href="#" data-tab-name="map" class="page-top__switcher tab-nav">
							<span class="icon-marker"></span>
						</a>

					</div>

				</div>
			</div>

		</div>

	</div>

	<div class="page-section">

		<div class="page-content">

			<h1 class="visuallyhidden">Новостройки</h1>

			<div class="page-loop__wrapper loop tab-content tab-content__active">
				<?php
				$houseArgs = array(
					'post_type'      => 'residential_complex',
					'posts_per_page' => - 1,
				);
				$house     = new WP_Query( $houseArgs ); ?>
				<?php if ( $house->have_posts() ): ?>
					<ul class="page-loop with-filter">

						<?php while ( $house->have_posts() ): $house->the_post() ?>

							<?php get_template_part( 'template-parts/card-build' ) ?>

						<?php endwhile; ?>
					</ul>
					<?php wp_reset_postdata(); ?>
				<?php endif; ?>

				<div class="show-more">

					<button class="show-more__button">

						<span class="show-more__button-icon"></span>

						Показать еще

					</button>

				</div>

			</div>

			<div class="page-map tab-content map">

				<h1>Тут будет карта</h1>

			</div>

		</div>

		<div class="page-filter fixed">

			<div class="page-filter__wrapper">

				<form id="page-filter" class="page-filter__form" method="POST">
					<div class="page-filter__body">
						<input type="hidden" name="action" value="ajax_filter">
						<?php $release = get_terms( array(
							'taxonomy'   => 'release',
							'hide_empty' => true,
						) ) ?>
						<?php if ( $release ) : ?>
							<div class="page-filter__category">
								<a href="#deadline" class="page-filter__category-link" data-toggle="collapse">
									<h3 class="page-title-h3"><?php _e('Срок сдачи', 'metro-city') ?></h3>
									<svg width="13" height="8" viewBox="0 0 13 8" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M6.036 0.611083L0.191897 6.45712C-0.0639745 6.71364 -0.0639745 7.12925 0.191897 7.38642C0.44777 7.64294 0.863375 7.64294 1.11925 7.38642L6.49964 2.00408L11.88 7.38577C12.1359 7.64229 12.5515 7.64229 12.808 7.38577C13.0639 7.12925 13.0639 6.713 12.808 6.45648L6.96399 0.610435C6.71076 0.357856 6.28863 0.357856 6.036 0.611083Z" fill="#111111"/>
									</svg>
								</a>
								<div class="page-filter__category-list collapse show" id="deadline">
									<ul class="deadline">
										<li>
											<div class="radio">
												<input type="radio" name="deadline" id="all" value="" checked>
												<label for="all"><?php _e('Любой', 'metro-city') ?></label>
											</div>
										</li>
										<?php foreach ( $release as $item ) : ?>
											<li>
												<div class="radio">
													<input type="radio" name="deadline" id="<?php echo $item->slug ?>" value="<?php echo $item->slug ?>">
													<label for="<?php echo $item->slug ?>"><?php echo $item->name ?></label>
												</div>
											</li>
										<?php endforeach; ?>

									</ul>
								</div>
							</div>
						<?php endif; ?>

						<?php $housing = get_terms( array(
							'taxonomy'   => 'class',
							'hide_empty' => true,
						) ) ?>

						<?php if ( $housing ) : ?>
							<div class="page-filter__category">

								<a href="#housing" class="page-filter__category-link" data-toggle="collapse">
									<h3 class="page-title-h3"><?php _e( 'Класс жилья', 'metro-city' ) ?></h3>
									<svg width="13" height="8" viewBox="0 0 13 8" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M6.036 0.611083L0.191897 6.45712C-0.0639745 6.71364 -0.0639745 7.12925 0.191897 7.38642C0.44777 7.64294 0.863375 7.64294 1.11925 7.38642L6.49964 2.00408L11.88 7.38577C12.1359 7.64229 12.5515 7.64229 12.808 7.38577C13.0639 7.12925 13.0639 6.713 12.808 6.45648L6.96399 0.610435C6.71076 0.357856 6.28863 0.357856 6.036 0.611083Z" fill="#111111"/>
									</svg>
								</a>

								<div class="page-filter__category-list collapse show" id="housing">
									<ul class="housing">
										<?php foreach ( $housing as $item ) : ?>
											<li>
												<div class="checkbox">
													<input type="checkbox" name="category[]" id="<?php echo $item->slug; ?>" value="<?php echo $item->slug; ?>">
													<label for="<?php echo $item->slug; ?>"><?php echo $item->name; ?></label>
												</div>
											</li>
										<?php endforeach; ?>
									</ul>
								</div>
							</div>
						<?php endif; ?>

						<?php $additional = get_terms( array(
							'taxonomy'   => 'options',
							'hide_empty' => true
						) ) ?>
						<?php if ( $additional ) : ?>
							<div class="page-filter__category">

								<a href="#additional" class="page-filter__category-link" data-toggle="collapse">
									<h3 class="page-title-h3"><?php _e( 'Дополнительные опции', 'metro-city' ) ?></h3>
									<svg width="13" height="8" viewBox="0 0 13 8" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M6.036 0.611083L0.191897 6.45712C-0.0639745 6.71364 -0.0639745 7.12925 0.191897 7.38642C0.44777 7.64294 0.863375 7.64294 1.11925 7.38642L6.49964 2.00408L11.88 7.38577C12.1359 7.64229 12.5515 7.64229 12.808 7.38577C13.0639 7.12925 13.0639 6.713 12.808 6.45648L6.96399 0.610435C6.71076 0.357856 6.28863 0.357856 6.036 0.611083Z" fill="#111111"/>
									</svg>
								</a>

								<div class="page-filter__category-list collapse show" id="additional">
									<ul class="additional">
										<?php foreach ( $additional as $key => $item ) : ?>
											<?php if ( $key <= 5 ) : ?>
												<li>
													<div class="checkbox">
														<input type="checkbox" name="options[]" id="<?php echo $item->slug; ?>" value="<?php echo $item->slug; ?>">
														<label for="<?php echo $item->slug; ?>"><?php echo $item->name; ?></label>
													</div>
												</li>
											<?php endif; ?>

										<?php endforeach; ?>
									</ul>
									<?php if ( count( $additional ) >= 4 ) : ?>
										<div class="collapse" id="additional_collapse">
											<ul class="additional additional__collapse">
												<?php foreach ( $additional as $key => $item ) : ?>
													<?php if ( $key >= 5 ) : ?>
														<li>
															<div class="checkbox">
																<input type="checkbox" name="options[]" id="<?php echo $item->slug; ?>" value="<?php echo $item->slug; ?>">
																<label for="<?php echo $item->slug; ?>"><?php echo $item->name; ?></label>
															</div>
														</li>
													<?php endif; ?>
												<?php endforeach; ?>
											</ul>
										</div>
									<?php endif; ?>

									<?php if ( count( $additional ) >= 4 ) : ?>
										<a href="#additional_collapse" class="page-filter__category-more" data-toggle="collapse" data-count="<?php echo intval( count( $additional ) - 4 ) ?>" role="button"><?php echo __( 'Показать еще' ) . ' (' . intval( count( $additional ) - 4 ) . ')' ?></a>
									<?php endif; ?>

								</div>
							</div>
						<?php endif; ?>

					</div>

					<div class="page-filter__buttons">

						<button class="button button--pink w-100" type="submit" id="apply_filter">Применить фильтры
						</button>

						<button class="button w-100" type="reset" id="reset_filter">Сбросить фильтры
							<svg width="9" height="8" viewBox="0 0 9 8" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M8.5 0.942702L7.5573 0L4.49999 3.05729L1.4427 0L0.5 0.942702L3.55729 3.99999L0.5 7.0573L1.4427 8L4.49999 4.94271L7.55728 8L8.49998 7.0573L5.44271 3.99999L8.5 0.942702Z"/>
							</svg>
						</button>

					</div>

				</form>

			</div>

		</div>

	</div>

</div>
<?php get_footer(); ?>
