$(document).ready(function (){

	let form = $("#page-filter");

	// ajax request
	function ajax_request(data){
		$.ajax({
			url: custom.ajax_url,
			type: 'POST',
			data: data,
			beforeSend: function (){
				$('.preloader').fadeIn();
			},
			success: function (responce) {
				$('ul.with-filter').html(responce);
				$('.preloader').fadeOut();
			},
		})
	}

	// ajax filter
	$("#page-filter button[type='submit']").on('click', function (e){
		e.preventDefault();
		let serialized = form.serialize();
		ajax_request(serialized)
	})
	// ajax reset
	$("#reset_filter").on('click', function (){
		form[0].reset();
		let serialized = form.serialize();
		ajax_request(serialized);
	})

})
