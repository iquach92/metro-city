<?php

add_action('wp_enqueue_scripts', 'init_scripts_and_styles');
function init_scripts_and_styles() {

	wp_enqueue_style('icon-font', get_template_directory_uri() . '/fonts/icomoon/icon-font.css');
	wp_enqueue_style('custom', get_template_directory_uri() . '/css/custom.css');
	wp_enqueue_style('main', get_template_directory_uri() . '/css/style.min.css');

	wp_deregister_script( 'jquery' );
	wp_deregister_script( 'jquery-migrate' );

	wp_enqueue_script( 'jquery', 'https://code.jquery.com/jquery-3.5.1.min.js', '','3.5.1', true );
	wp_enqueue_script( 'jquery-migrate', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.3.1/jquery-migrate.min.js', '','3.3.1', true );

	wp_enqueue_script('bootstrap-popper', get_template_directory_uri() . '/libs/bootstrap/js/popper.min.js', '','',true);
	wp_enqueue_script('bootstrap', get_template_directory_uri() . '/libs/bootstrap/js/bootstrap.min.js', '','',true);
	wp_enqueue_script('ofi', get_template_directory_uri() . '/libs/ofi/ofi.min.js', '','',true);
	wp_enqueue_script('wowjs', get_template_directory_uri() . '/libs/wowjs/wow.min.js', '','',true);
	wp_enqueue_script('yandex-map', 'https://api-maps.yandex.ru/2.1/?apikey=f7f5866c-fcab-4da8-94d7-cdbdb39c7d22&lang=ru_RU', '','',true);


	wp_enqueue_script( 'main', get_template_directory_uri() . '/js/scripts.js', '', '1.0', true );
	wp_localize_script( 'main', 'main', array( 'directory_url' => get_template_directory_uri(), ) );

	wp_enqueue_script( 'ajax', get_template_directory_uri() . '/js/ajax.js', '', '1.0', true );
	wp_localize_script( 'ajax', 'custom', array( 'ajax_url' => admin_url('admin-ajax.php'), ) );

}
